const express = require("express");
const app = express();

// middleware
app.use(express.static("public"));

//server on port 5000
app.listen(5000, () => {
  console.log("server started on port 5000");
});